import { Module } from 'vuex'
import { ILoginState } from './types'
import { IRootState } from '../types'

import router from '@/router'

import localCache from '@/utils/localCache'
import { mapMenusToRoutes } from '@/utils/mapRoutes'
import {
  accountLoginRequest,
  requestUserInfoById,
  requestUserMenusByRoleId
} from '@/service/login/login'
import {
  CHANGE_TOKEN,
  CHANGE_USERINFO,
  CHANGE_ROLE_MENUS,
  ACCOUNT_LOGIN_ACTION,
  SETUP_LOGIN_ACTION
} from './constants'

const loginModule: Module<ILoginState, IRootState> = {
  namespaced: true,
  state() {
    return {
      token: '',
      userInfo: {},
      roleMenus: []
    }
  },
  mutations: {
    [CHANGE_TOKEN](state, token: string) {
      state.token = token
    },
    [CHANGE_USERINFO](state, userInfo: any) {
      state.userInfo = userInfo
    },
    [CHANGE_ROLE_MENUS](state, roleMenus: any[]) {
      state.roleMenus = roleMenus
      const routes = mapMenusToRoutes(roleMenus)
      routes.forEach((route) => {
        router.addRoute('main', route)
      })
    }
  },
  actions: {
    async [ACCOUNT_LOGIN_ACTION]({ commit }, payload: any) {
      // 登录逻辑
      const loginResult = await accountLoginRequest(payload)
      const { id, token } = loginResult.data
      commit(CHANGE_TOKEN, token)
      localCache.setCache('token', token)

      // 获取用户信息
      const userResult = await requestUserInfoById(id)
      const userInfo = userResult.data
      commit(CHANGE_USERINFO, userInfo)
      localCache.setCache('userInfo', userInfo)

      // 获取菜单信息
      const roleMenusResult = await requestUserMenusByRoleId(userInfo.role.id)
      const roleMenus = roleMenusResult.data
      commit(CHANGE_ROLE_MENUS, roleMenus)
      localCache.setCache('roleMenus', roleMenus)
      // 权限方法
      const routes = mapMenusToRoutes(roleMenus)
      routes.forEach((route) => {
        router.addRoute('main', route)
      })
      // 页面跳转
      router.replace('/main')
    },
    // 初始化用户登录信息（浏览器刷新调用）
    [SETUP_LOGIN_ACTION]({ commit }) {
      const token = localCache.getCache('token')
      token && commit(CHANGE_TOKEN, token)
      const userInfo = localCache.getCache('userInfo')
      userInfo && commit(CHANGE_USERINFO, userInfo)
      const roleMenus = localCache.getCache('roleMenus')
      roleMenus && commit(CHANGE_ROLE_MENUS, roleMenus)
    }
  },
  getters: {}
}

export default loginModule
