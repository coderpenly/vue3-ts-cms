export const CHANGE_TOKEN = '/login/changeToken'
export const CHANGE_USERINFO = '/login/changeUserInfo'
export const CHANGE_ROLE_MENUS = '/login/changeRoleMenus'

export const ACCOUNT_LOGIN_ACTION = '/login/accountLoginAction'
export const SETUP_LOGIN_ACTION = '/login/setupLoginAction'
