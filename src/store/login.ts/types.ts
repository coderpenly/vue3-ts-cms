export interface ILoginState {
  token: string
  userInfo: any
  roleMenus: any[]
}
