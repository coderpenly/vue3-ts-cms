import { createStore, Store, useStore as useVuexStore } from 'vuex'

import { IRootState, IStoreType } from './types'

import login from './login.ts/login'
import { SETUP_LOGIN_ACTION } from './login.ts/constants'

const store = createStore<IRootState>({
  state() {
    return {
      name: 'coderYang',
      password: '',
      height: ''
    }
  },
  mutations: {},
  actions: {},
  getters: {},
  modules: {
    login
  }
})

export function setupState() {
  store.dispatch(`login/${SETUP_LOGIN_ACTION}`)
}

export function useStore(): Store<IStoreType> {
  return useVuexStore()
}
export default store
