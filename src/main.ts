import { createApp } from 'vue'
import App from './App.vue'

import 'normalize.css'
import './assets/css/index.css'

import registerApp from './global'
import router from './router'
import store, { setupState } from './store'

const app = createApp(App)

app.use(registerApp)
app.use(store)
// 为了好看才这么写的，别误会~，内部没用到 app 对象
// 单纯是为了好看~
// 需要先执行 setup，然后在 use router，解决bug
app.use(setupState)
app.use(router)

app.mount('#app')
