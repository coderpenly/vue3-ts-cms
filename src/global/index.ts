import { App } from 'vue'
import registorElement from './registor-element'

export default function registorApp(app: App): void {
  registorElement(app)
}
