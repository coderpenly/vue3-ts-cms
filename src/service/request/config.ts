let BASE_URL = ''
const TIME_OUT = 10000

switch (process.env.NODE_ENV) {
  case 'development':
    BASE_URL = '/api'
    break
  case 'test':
    BASE_URL = 'http://localhost:8000'
    break
  case 'production':
    BASE_URL = 'http://production.com'
    break
  default:
    BASE_URL = 'http://123.207.32.32:8000'
}

export { BASE_URL, TIME_OUT }
