import type { AxiosRequestConfig, AxiosResponse } from 'axios'
export interface ICyRequestInterceptors<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (err: any) => any
  responseInterceptor?: (res: T) => T
  responseInterceptorCatch?: (err: any) => any
}
export interface ICyRequectConfig<T = AxiosResponse>
  extends AxiosRequestConfig {
  interceptors?: ICyRequestInterceptors<T>
  showLoading?: boolean
}
