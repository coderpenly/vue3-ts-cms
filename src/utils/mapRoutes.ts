import { RouteRecordRaw } from 'vue-router'
import { IBreadItem } from '@/base-ui/breadcrumb'

let firstMenu: any

export function mapMenusToRoutes(roleMenus: any[]): RouteRecordRaw[] {
  const routes: RouteRecordRaw[] = []

  // 加载所有的文件，并配置成对应的 route
  const allRoutes: RouteRecordRaw[] = []
  const routeFiles = require.context('../router/main', true, /\.ts/)
  routeFiles.keys().forEach((item) => {
    const route = require('../router/main' + item.split('.')[1])
    allRoutes.push(route.default)
  })

  // 根据菜单返回响应的 route
  const _recurseGetRoute = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 2) {
        const route = allRoutes.find((route) => route.path === menu.url)
        if (route) {
          routes.push(route)
          if (!firstMenu) {
            firstMenu = route
          }
        }
      } else {
        _recurseGetRoute(menu.children)
      }
    }
  }

  _recurseGetRoute(roleMenus)

  return routes
}

export function pathMapBreakcrumbs(userMenus: any[], currentPath: string) {
  const breadcrumbs: IBreadItem[] = []
  pathMapToMenus(userMenus, currentPath, breadcrumbs)
  return breadcrumbs
}
export function pathMapToMenus(
  userMenus: any[],
  currentPath: string,
  breadcrumbs?: IBreadItem[]
): any {
  for (const menu of userMenus) {
    if (menu.type === 1) {
      const findPath = pathMapToMenus(menu.children ?? [], currentPath)
      if (findPath) {
        breadcrumbs?.push({ name: menu.name })
        breadcrumbs?.push({ name: findPath.name })
        return findPath
      }
    } else if (menu.type === 2 && menu.url === currentPath) {
      return menu
    }
  }
}

export { firstMenu }
