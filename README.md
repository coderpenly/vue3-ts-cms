# vue3-ts-cms

## 项目简介

### 技术：

* Vue3
* Vue-router@next
* Vuex@next
* Axios
* TypeScript
* Element-plus
* Echars

### 目标：

* 搭建一个使用 vue3 + TypeScript 的管理系统，完成管理系统部分功能
* 项目搭建过程使用 eslint
* git 提交拦截，并统一规范



### 介绍

#### setup

```
npm install
```

#### 项目启动

```
npm run serve
```

#### 项目打包

```
npm run build
```

#### 项目格式化

```
npm run prettier
```

#### 项目提交

```
npm run commit
```



